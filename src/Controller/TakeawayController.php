<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TakeawayController extends AbstractController
{
    /**
     * @Route("/takeaway", name="takeaway")
     */
    public function index(): Response
    {
        return $this->render('takeaway/index.html.twig', [
            'controller_name' => 'TakeawayController',
        ]);
    }
}
