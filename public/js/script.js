// Lightbox

// Chargement du DOM
$(function() {

    // J'applique un ecouteur d'evenement sur toutes les images ayant la class lightbox
    $('img.lightbox').on('click', function() {

        // Je récupere la valeur de src
        let src = $(this).attr('src');

        // Je concatene la valeur de src dans une nouvelle balise
        let html = `<div class="view"><img src="${src}" alt="preview"></div>`;

        // J'insere le html dans "body"
        $('body').append(html);

        // Faire apparaitre la div "view", modifier le selecteur display en flex et j'applique
        $('div.view').fadeIn('slow').css('display', 'flex').on('click', function() {
            // Si il y a un click on fait disparaitre cette meme div
            $(this).fadeOut('slow', function() {
                // Une fois la div disparue on supprime le code html de la div
                $(this).remove();
            });
        });
    });
});